package com;

import java.util.ArrayList;
import java.util.List;



public class MyClass {
	private static List<Employee> empList;
	static {
		empList = new ArrayList<Employee>();
		empList.add(new Employee(1,"Sai Kishore",21,254584,"DE","ds"));
		empList.add(new Employee(2,"Kishore",21,354584,"DE","ds"));
		empList.add(new Employee(12,"Aria",21,154584,"DE","cs"));
		empList.add(new Employee(52,"Boria",21,54584,"DE","ds"));
	}
	public static void main(String[] args) {
		
		DataStructureA datastrcA = new DataStructureA();
		datastrcA.sortingNames((ArrayList<Employee>) empList);
		System.out.println("====  ====");
		DataStructureB dataB = new DataStructureB();
		dataB.cityNameCount((ArrayList<Employee>) empList);
		System.out.println("====  ====");
		dataB.monthlySalary((ArrayList<Employee>) empList);
		
	}


}

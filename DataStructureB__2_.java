package com;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class DataStructureB {
	public void cityNameCount(ArrayList<Employee> employees) {
		System.out.println("city" + " " + "count");
		employees.stream().collect(Collectors.groupingBy(Employee::getCity)).forEach((city, emps) -> {
		     int count = emps.size();		     
		     System.out.print(city + "    " + count);
		     System.out.println();
		 });
	}
	public void monthlySalary(ArrayList<Employee> employees) {
		for (Employee employee : employees) {
			System.out.println(employee.getId() + " = " +employee.getSalary()  );
		}
	}
		 
}
